#ifndef APPLICATION_H_
#define APPLICATION_H_
#include "BaseApplication.h"

class Application : public BaseApplication {
public:
	void setup() override;
	//void update();
	void draw() override;
};
#endif