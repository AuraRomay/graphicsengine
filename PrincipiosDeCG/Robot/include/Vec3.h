#pragma once
class Vec3
{
public:
	float puntos[4];
	float x() { return puntos[0]; }
	float y() { return puntos[1]; }
	float z() { return puntos[2]; }
	float w() { return puntos[3]; }

	Vec3(float, float, float, float);
	//Vec3 operator-(const Vec3& rh);
	Vec3();
	~Vec3();
};

