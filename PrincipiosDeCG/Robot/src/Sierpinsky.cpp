#include "Sierpinsky.h"




void Sierpinsky::generate(Vec3 a, Vec3 b, Vec3 c, int subdiv)
{
	if (subdiv == 0) {
		vertices.push_back(a);
		vertices.push_back(b);
		vertices.push_back(c);
	}
	else {
		Vec3 ab = midPoint(a, b);
		Vec3 bc = midPoint(b, c);
		Vec3 ca = midPoint(c, a);

		generate(a, ab, ca, subdiv - 1);
		generate(ab, b, bc, subdiv - 1);
		generate(ca, bc, c, subdiv -1);
	}
}

Vec3 Sierpinsky::midPoint(const Vec3& po,const  Vec3& pi)
{
	Vec3 temp;
	
	temp.puntos[0] = (pi.puntos[0] + po.puntos[0]) / 2;
	temp.puntos[1] = (pi.puntos[1] + po.puntos[1]) / 2;
	temp.puntos[2] = (pi.puntos[2] + po.puntos[2]) / 2;

	return temp;
}

Sierpinsky::Sierpinsky()
{
}


Sierpinsky::~Sierpinsky()
{
}
