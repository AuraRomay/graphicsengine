#ifndef APPLICATION_H_
#define APPLICATION_H_
#include "BaseApplication.h"

class Application : public BaseApplication {
public:
	void setup() override;
	//void update();
	void draw() override;
	void line(int x0, int x1, int y0, int y1);
	void lineTo(int x, int y);
	void moveTo(int x, int y);
	void clearScreen();

	private:
		int lados;
};
#endif