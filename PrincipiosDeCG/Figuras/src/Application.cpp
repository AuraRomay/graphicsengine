#define GLM_FORCE_RADIANS
#include "Application.h"
#include <iostream>
#include <math.h>

int CuX = 0;
int CuY = 0;

void Application::setup()
{
	std::cout << "¿Cuantos lados quieres?";
	std::cin >> lados;
}

//void BaseApplication::update()
//{
//}

void Application::draw()
{
	float Pi = 3.14159;
	float angulo = 0;
	int radio = 300;
	int Cx = WIDTH / 2;
	int Cy = HEIGHT / 2;

	moveTo((Cx + radio), (Cy));

	for (int i = 1; i <= lados; i ++) {
		int angulos = i * 360 / lados;
		int x = (radio * std::cos((angulos*(Pi / 180.0))))+Cx;
		int y = (radio * (-1 * std::sin((angulos*(Pi / 180.0)))))+Cy;
		lineTo(x, y);
	}

}

void Application::line(int x0, int x1, int y0, int y1) {

	int y = y0;
	int x = x0;
	
	int dx = x1 - x0;
	int dy = y1 - y0;
	
	// 1
	if (dx >= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

	int A = (y1 - y0);
	int B = (x1 - x0);
	int dInicial = (A + B / 2); //el punto de partida del algoritmo
	int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		
		}
	}
	//2
	else if (dx >= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}
	//3
	else if (dx <= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 255, 0, 255);
			
		}
	}
	//4
	else if (dx <= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

		int A = (y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
			
		}
	}
	//5
	else if (dx <= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}
	//6
	else  if (dx <= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 255, 0, 255);
			
		}
	}
	//7
	else if (dx >= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 255, 0, 0, 255);
			
		}
	}
	//8
	else if (dx >= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = (x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}		

	
}

void Application::lineTo(int x, int y) {
	line(CuX, x, CuY, y);
	moveTo(x, y);
}

void Application::moveTo(int x, int y) {
	int Cx = WIDTH / 2;
	int Cy = HEIGHT / 2;
	CuX = x;
	CuY = y;
}

void Application::clearScreen() 
{
	/*for (int i = 0; i < BaseApplication::WIDTH * BaseApplication::HEIGHT * BaseApplication::RGBA; i+=1) {
		BaseApplication::_screenBuffer[i] = 0;
	}*/
	for (int i = 0; i < 1024; i++) {
		for (int l = 0; l < 1024; l++) {
			putPixel(l, i, 255, 0, 0, 0);
		}
	}
}

