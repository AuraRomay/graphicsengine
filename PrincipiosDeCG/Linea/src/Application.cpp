#define GLM_FORCE_RADIANS
#include "Application.h"
#include <iostream>
#include <math.h>  

void Application::setup()
{
}

//void BaseApplication::update()
//{
//}

void Application::draw()
{
	float Pi = 3.14159;
	float angulo = 0;
	int Cx = WIDTH / 2;
	int Cy = HEIGHT / 2;

		//line(100, 1020, 50, 950);

		for (int angle = 0; angle < 360; angle++) {
			int x = 300 * std::cos((angle*(Pi / 180.0)));
			int y = 300 * (-1*std::sin((angle*(Pi / 180.0))));
			line(Cx, x + Cx, Cy, y + Cy);
		}

}

void Application::line(int x0, int x1, int y0, int y1) {

	int y = y0;
	int x = x0;
	
	int dx = x1 - x0;
	int dy = y1 - y0;
	
	// 1
	if (dx >= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

	int A = (y1 - y0);
	int B = (x1 - x0);
	int dInicial = (A + B / 2); //el punto de partida del algoritmo
	int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		
		}
	}
	//2
	else if (dx >= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}
	//3
	else if (dx <= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 255, 0, 255);
			
		}
	}
	//4
	else if (dx <= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

		int A = (y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
			
		}
	}
	//5
	else if (dx <= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}
	//6
	else  if (dx <= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 255, 0, 255);
			
		}
	}
	//7
	else if (dx >= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 255, 0, 0, 255);
			
		}
	}
	//8
	else if (dx >= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = (x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x , y , 0, 0, 255, 255);
			
		}
	}		

	
}

