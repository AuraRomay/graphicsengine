#pragma once
class Vec4
{
public:
	float puntos[4];
	float x() { return puntos[0]; }
	float y() { return puntos[1]; }
	float z() { return puntos[2]; }
	float w() { return puntos[3]; }

	Vec4(float X, float Y, float Z, float W);
	Vec4 cross(Vec4 a, Vec4 b);
	Vec4 normalize(Vec4 a);
	Vec4 operator-(const Vec4& rh)const;
	Vec4();
	~Vec4();
};
