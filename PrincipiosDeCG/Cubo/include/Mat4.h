#pragma once
#include "Vec4.h"

class Mat4
{
public:
	float puntos[4][4];
	Mat4();
	
	static Mat4 Identity();
	static Mat4 Translate(const float &Tx, const float &Ty, const float & Tz);
	static Mat4 Translate(const Vec4& v);
	static Mat4 RotateX(const int angle);
	static Mat4 RotateY(const int angle);
	static Mat4 RotateZ(const int angle);

	Mat4 LookAt(Vec4 eye, Vec4 target, Vec4 up);
	Mat4 Perspective(const float & fov, const float & aspect, const float & near, const float & far);

	Mat4 operator*(const Mat4 &rh);
	Vec4 operator*(const Vec4& rh);
	~Mat4();
};