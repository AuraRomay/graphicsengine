#include "Mat3.h"
#include "math.h"

/*Mat3::Mat3(const float & valve)
{
	//this->Identity();
}*/

Mat3::Mat3()
{
	puntos[0][0] = 1;
	puntos[1][1] = 1;
	puntos[2][2] = 1;
	puntos[3][3] = 1;
	puntos[0][1] = 0;
	puntos[0][2] = 0;
	puntos[0][3] = 0;
	puntos[1][0] = 0;
	puntos[1][2] = 0;
	puntos[1][3] = 0;
	puntos[2][0] = 0;
	puntos[2][1] = 0;
	puntos[2][3] = 0;
	puntos[3][0] = 0;
	puntos[3][1] = 0;
	puntos[3][2] = 0;
}

Mat3 & Mat3::Identity()
{
	Mat3 mat;
	mat.puntos[0][0] = 1;
	mat.puntos[1][1] = 1;
	mat.puntos[2][2] = 1;
	mat.puntos[3][3] = 1;
	return mat;
}

Mat3 & Mat3::Translate(const float & Tx, const float & Ty)
{
	// TODO: insert return statement here
	Mat3 result;
	result.puntos[0][3] = Tx;
	result.puntos[1][3] = Ty;
	result.puntos[0][0] = 1;
	result.puntos[1][1] = 1;
	result.puntos[2][2] = 1;
	result.puntos[3][3] = 1;
	result.puntos[0][1] = 0;
	result.puntos[0][2] = 0;
	result.puntos[1][0] = 0;
	result.puntos[1][2] = 0;
	result.puntos[2][0] = 0;
	result.puntos[2][1] = 0;
	result.puntos[2][3] = 0;
	result.puntos[3][0] = 0;
	result.puntos[3][1] = 0;
	result.puntos[3][2] = 0;
	return result;
}

Mat3 & Mat3::Translate(const Vec3 & v)
{
	// TODO: insert return statement here
	return Mat3::Translate(v.puntos[0], v.puntos[1]);
}

Mat3 & Mat3::Rotate(const int angle)
{
	// TODO: insert return statement here
	Mat3 result;
	result.puntos[0][0] = result.puntos[1][1] = cos(angle * 3.141594 /180);
	result.puntos[1][0] = sin(angle * 3.141594 / 180);
	result.puntos[0][1] = -sin(angle * 3.141594 / 180);
	return result;
}


Mat3 Mat3::operator*(const Mat3 &rh)
{
	Mat3 result;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; ++k) {
				result.puntos[i][j] += (puntos[i][k] * rh.puntos[k][j]);
			}	
			/*result.puntos[i][j] = total;
			total = 0;*/
		}
	}
	return result;
}

Vec3 Mat3::operator*(const Vec3 & rh)
{
	Vec3 result = Vec3();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			
		result.puntos[i] += puntos[i][j] * rh.puntos[j];
		}
	}
	return result;
}

Mat3::~Mat3()
{
}
