#include "Mat4.h"
#include <math.h>

Mat4::Mat4()
{
	puntos[0][0] = 1;
	puntos[1][1] = 1;
	puntos[2][2] = 1;
	puntos[3][3] = 1;
	puntos[0][1] = 0;
	puntos[0][2] = 0;
	puntos[0][3] = 0;
	puntos[1][0] = 0;
	puntos[1][2] = 0;
	puntos[1][3] = 0;
	puntos[2][0] = 0;
	puntos[2][1] = 0;
	puntos[2][3] = 0;
	puntos[3][0] = 0;
	puntos[3][1] = 0;
	puntos[3][2] = 0;
}

Mat4  Mat4::Identity()
{
	Mat4 mat;
	mat.puntos[0][0] = 1;
	mat.puntos[1][1] = 1;
	mat.puntos[2][2] = 1;
	mat.puntos[3][3] = 1;
	return mat;
}

Mat4  Mat4::Translate(const float & Tx, const float & Ty, const float & Tz)
{
	// TODO: insert return statement here
	Mat4 result;
	result.puntos[0][3] = Tx;
	result.puntos[1][3] = Ty;
	result.puntos[0][0] = 1;
	result.puntos[1][1] = 1;
	result.puntos[2][2] = 1;
	result.puntos[3][3] = 1;
	result.puntos[0][1] = 0;
	result.puntos[0][2] = 0;
	result.puntos[1][0] = 0;
	result.puntos[1][2] = 0;
	result.puntos[2][0] = 0;
	result.puntos[2][1] = 0;
	result.puntos[2][3] = 0;
	result.puntos[3][0] = 0;
	result.puntos[3][1] = 0;
	result.puntos[3][2] = 0;
	return result;
}

Mat4  Mat4::Translate(const Vec4 & v)
{
	// TODO: insert return statement here
	return Mat4::Translate(v.puntos[0], v.puntos[1], v.puntos[2]);
}

Mat4  Mat4::RotateZ(const int angle)
{
	// TODO: insert return statement here
	Mat4 result;
	result.puntos[0][0] = result.puntos[1][1] = cos(angle * 3.141594 / 180);
	result.puntos[1][0] = sin(angle * 3.141594 / 180);
	result.puntos[0][1] = -sin(angle * 3.141594 / 180);
	return result;
}
Mat4  Mat4::RotateY(const int angle)
{
	// TODO: insert return statement here
	Mat4 result;
	result.puntos[0][0] = result.puntos[2][2] = cos(angle * 3.141594 / 180);
	result.puntos[2][0] = -sin(angle * 3.141594 / 180);
	result.puntos[0][2] = sin(angle * 3.141594 / 180);
	return result;
}
Mat4  Mat4::RotateX(const int angle)
{
	// TODO: insert return statement here
	Mat4 result;
	result.puntos[1][1] = result.puntos[2][2] = cos(angle * 3.141594 / 180);
	result.puntos[1][2] = -sin(angle * 3.141594 / 180);
	result.puntos[2][1] = sin(angle * 3.141594 / 180);
	return result;
}


Mat4 Mat4::operator*(const Mat4 &rh)
{
	Mat4 result;
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			result.puntos[i][j] = 0;
		}
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; ++k) {
				result.puntos[i][j] += (puntos[i][k] * rh.puntos[k][j]);
			}
			/*result.puntos[i][j] = total;
			total = 0;*/
		}
	}
	return result;
}

Vec4 Mat4::operator*(const Vec4 & rh)
{
	Vec4 result = Vec4();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {

			result.puntos[i] += puntos[i][j] * rh.puntos[j];
		}
	}
	return result;
}

Mat4 Mat4::LookAt(Vec4 eye, Vec4 target, Vec4 up)
{
	Mat4 t, re, ro;
	Vec4 F = eye.normalize(target - eye);//z
	Vec4 R = eye.normalize(eye.cross(F, up));//x
	Vec4 U = eye.normalize(eye.cross(R, F));//y
	for (int i = 0; i < 3; ++i) {
		t.puntos[i][3] = -eye.puntos[i];
	}
	for (int i = 0; i < 3; ++i) {
		ro.puntos[0][i] = R.puntos[i];
		ro.puntos[1][i] = U.puntos[i];
		ro.puntos[2][i] = F.puntos[i];
	}
	re = ro * t;
	return re;
}

Mat4 Mat4::Perspective(const float & fov, const float & aspect, const float & near, const float & far)
{
	float y = cosf(fov* 3.141594 / 180);
	if (y != 0) {
		y = sinf(fov* 3.141594 / 180) / y;
		if (y != 0) {
			y = 1.0F / y;
		}
	}
	Mat4 m;
	m.puntos[0][0] = y / aspect;
	m.puntos[1][1] = y;
	m.puntos[2][2] = -((far + near) / (far - near));
	m.puntos[2][3] = -((2 * far*near) / (far - near));
	m.puntos[3][2] = 1;
	return m;
}

Mat4::~Mat4()
{
}