#include "Vec4.h"
#include <math.h>


Vec4::Vec4(float x, float y, float z, float w)
{
	this->puntos[0] = x;
	this->puntos[1] = y;
	this->puntos[2] = z;
	this->puntos[3] = w;

}

Vec4::Vec4()
{
	this->puntos[0] = 0;
	this->puntos[1] = 0;
	this->puntos[2] = 0;
	this->puntos[3] = 0;
}

Vec4 Vec4::cross(Vec4 a, Vec4 b) {
	float i, j, k;
	i = (a.puntos[1] * b.puntos[2]) - (a.puntos[2] * b.puntos[1]);
	j = (a.puntos[3] * b.puntos[0]) - (a.puntos[0] * b.puntos[3]);
	k = (a.puntos[0] * b.puntos[1]) - (a.puntos[1] * b.puntos[0]);

	return Vec4(i, j, k, 1);
}

Vec4 Vec4::normalize(Vec4 a) {
	Vec4 r;
	float l = sqrt((a.puntos[0] * a.puntos[0]) + (a.puntos[1] * a.puntos[1]) + (a.puntos[2] * a.puntos[2]));
	for (int i = 0; i < 3; ++i) {
		r.puntos[i] = a.puntos[i] / l;
	}
	return r;
}

Vec4 Vec4::operator-(const Vec4& rh)const //<-VALOR DE IZQ NO SE MODIFICA, ENTREGA VALOR NUEVO
{
	return Vec4(puntos[0] - rh.puntos[0], puntos[1] - rh.puntos[1], puntos[2] - rh.puntos[2], 1);
}

Vec4::~Vec4()
{
}
