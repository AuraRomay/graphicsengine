#define GLM_FORCE_RADIANS
#include "Application.h"
#include "Mat3.h"
#include "Vec3.h"
#include "Mat4.h"
#include "Vec4.h"
#include <iostream>
#include <math.h>

int CuX = 0;
int CuY = 0;


void Application::setup()
{
	srand(time(NULL));
	for (int i = 0; i < 15; ++i) {
		x.push_back(rand() % WIDTH);
		y.push_back(rand() % HEIGHT);
		r.push_back(rand() % 500);
	}

}

void Application::update()
{
	
}

void Application::draw()
{
	for (int i = 0; i < 15; ++i) {
		MidPointCircle(x[i], y[i], r[i]);
	}
		
}

void Application::line(int x0, int x1, int y0, int y1) {

	int y = y0;
	int x = x0;
	
	int dx = x1 - x0;
	int dy = y1 - y0;
	
	// 1
	if (dx >= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

	int A = (y1 - y0);
	int B = (x1 - x0);
	int dInicial = (A + B / 2); //el punto de partida del algoritmo
	int d = dInicial;
	putPixel(x, y, 255, 0, 0, 255);
		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		
		}
	}
	//2
	else if (dx >= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//3
	else if (dx <= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//4
	else if (dx <= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

		int A = (y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//5
	else if (dx <= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//6
	else  if (dx <= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}
	//7
	else if (dx >= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}
	//8
	else if (dx >= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = (x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}		

	
}

void Application::lineTo(int x, int y) {
	line(CuX, x, CuY, y);
	moveTo(x, y);
}

void Application::moveTo(int x, int y) {
//	int Cx = WIDTH / 2;
//	int Cy = HEIGHT / 2;
	CuX = x;
	CuY = y;
}

void Application::drawTriangle(Vec4 a, Vec4 b, Vec4 c)
{
	moveTo(a.x(), a.y());
	lineTo(b.x(), b.y());
	lineTo(c.x(), c.y());
	lineTo(a.x(), a.y());
}

void Application::clearScreen() {
	for (int i = 0; i < HEIGHT-1; i++) {
		for (int l = 0; l < WIDTH-1; l++) {
			putPixel_0(l, i, 0, 0, 0, 0);
		}
	}
	/*for (int i = 0; i < BaseApplication::WIDTH * BaseApplication::HEIGHT * BaseApplication::RGBA; i += 1) {
		BaseApplication::_screenBuffer[i] = 0;
	}*/
}

void Application::setColor(unsigned char R, unsigned char G, unsigned char B, unsigned char A)
{
	Red = R;
	Green = G;
	Blue = B;
	Alpha = A;
}

void Application::cubo(Vec4 a, Vec4 b, Vec4 c, Vec4 d, Vec4 e, Vec4 f, Vec4 g, Vec4 h) {
	vert4.push_back(a);
	vert4.push_back(b);
	vert4.push_back(c);
	vert4.push_back(a);
	vert4.push_back(b);
	vert4.push_back(f);
	vert4.push_back(a);
	vert4.push_back(e);
	vert4.push_back(f);
	vert4.push_back(a);
	vert4.push_back(e);
	vert4.push_back(g);
	vert4.push_back(a);
	vert4.push_back(c);
	vert4.push_back(g);
	vert4.push_back(b);
	vert4.push_back(c);
	vert4.push_back(d);
	vert4.push_back(b);
	vert4.push_back(d);
	vert4.push_back(f);
	vert4.push_back(c);
	vert4.push_back(d);
	vert4.push_back(g);
	vert4.push_back(d);
	vert4.push_back(g);
	vert4.push_back(h);
	vert4.push_back(d);
	vert4.push_back(f);
	vert4.push_back(h);
	vert4.push_back(e);
	vert4.push_back(g);
	vert4.push_back(h);
	vert4.push_back(e);
	vert4.push_back(f);
	vert4.push_back(h);
}

void Application::MidPointCircle(int cx, int cy, int r) {
	int x = 0;
	int y = r;
	int d = 1 - r;
	while (x < y)
	{
		putPixel_0(x + cx, y + cy, 255, 255, 255, 255);
		putPixel_0(y + cx, x + cy, 255, 255, 255, 255);
		putPixel_0(x + cx, -y + cy, 255, 255, 255, 255);
		putPixel_0(-y + cx, x + cy, 255, 255, 255, 255);
		putPixel_0(-x + cx, y + cy, 255, 255, 255, 255);
		putPixel_0(y + cx, -x + cy, 255, 255, 255, 255);
		putPixel_0(-x + cx, -y + cy, 255, 255, 255, 255);
		putPixel_0(-y + cx, -x + cy, 255, 255, 255, 255);

		if (d < 0) {
			d += 2 * x + 3;
		}
		else
		{
			--y;
			d += ((2 * x) - (2 * y)) + 5;
		}
		++x;
	}
}

