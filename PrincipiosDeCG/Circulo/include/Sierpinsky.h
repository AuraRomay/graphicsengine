#pragma once
#include "Vec3.h"
#include <vector>

class Sierpinsky
{
public:
	std::vector<Vec3>vertices;
	void generate(Vec3 a, Vec3 b, Vec3 c, int subdiv);
	Vec3 midPoint(const Vec3& po, const Vec3& pi);
	int subdiv_ = 0;

	Sierpinsky();
	~Sierpinsky();
};

