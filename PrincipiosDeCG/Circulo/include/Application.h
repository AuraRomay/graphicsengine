#ifndef APPLICATION_H_
#define APPLICATION_H_
#include "BaseApplication.h"
#include "Sierpinsky.h"
#include "Mat3.h"
#include "Mat4.h"
#include <vector>
#include <stdlib.h>
#include <time.h>

class Application : public BaseApplication {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void line(int x0, int x1, int y0, int y1);
	void lineTo(int x, int y);
	void moveTo(int x, int y);
	void clearScreen();
	void setColor(unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void cubo(Vec4 a, Vec4 b, Vec4 c, Vec4 d, Vec4 e, Vec4 f, Vec4 g, Vec4 h);
	void drawTriangle(Vec4 a, Vec4 b, Vec4 c);
	void MidPointCircle(int cx, int cy, int r);

	private:
		int lados;
		int Red;
		int Green;
		int Blue;
		int Alpha;
		int Cx = (int)WIDTH / 2;
		int Cy = (int)HEIGHT / 2;
		int ang = 0;
		float ang1 = 0;
		float Rand1 = 0;
		float Rand2 = 0;
		float Radio = 0;
		Sierpinsky sierpin;
		Mat3 m;
		Mat4 m4;
		std::vector<Vec3>vert;
		std::vector<Vec4>vert4;
		std::vector<Vec4>vertC;
		std::vector<int>x;
		std::vector<int>y;
		std::vector<int>r;
		Vec4 target, *eye, up;
};
#endif