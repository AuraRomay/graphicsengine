#pragma once
#include "Vec3.h"

class Mat3
{
public:
	float puntos[4][4];
	//Mat3(const float& valve = 1); //1= IDENTIDAD
	Mat3();

	static Mat3& Identity();
	static Mat3& Translate(const float &Tx, const float &Ty);
	static Mat3& Translate(const Vec3& v);
	static Mat3& Rotate(const int angle);

	Mat3 operator*(const Mat3 &rh);
	Vec3 operator*(const Vec3& rh);
	~Mat3();
};

