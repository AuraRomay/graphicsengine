#ifndef APPLICATION_H_
#define APPLICATION_H_
#include "BaseApplication.h"
#include "Sierpinsky.h"
#include "Mat3.h"
#include <vector>


class Application : public BaseApplication {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void line(int x0, int x1, int y0, int y1);
	void lineTo(int x, int y);
	void moveTo(int x, int y);
	void clearScreen();
	void setColor(unsigned char R, unsigned char G, unsigned char B, unsigned char A);

	private:
		int lados;
		int Red;
		int Green;
		int Blue;
		int Alpha;
		Sierpinsky sierpin;
		Mat3 m;
		int Cx = (int)WIDTH / 2;
		int Cy = (int)HEIGHT / 2;
		int ang = 0;
		std::vector<Vec3>vert;
};
#endif