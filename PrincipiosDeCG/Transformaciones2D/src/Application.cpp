#define GLM_FORCE_RADIANS
#include "Application.h"
#include "Mat3.h"
#include "Vec3.h"
#include <iostream>
#include <math.h>

int CuX = 0;
int CuY = 0;


void Application::setup()
{
	std::cout << "¿Cuantas subdivisiones quieres?";
	std::cin >> sierpin.subdiv_;

	Vec3 a(0, 300,0, 1);
	Vec3 b(-300, -300, 0, 1);
	Vec3 c(300, -300, 0, 1);

	sierpin.generate(a, b, c, sierpin.subdiv_);
	
}

void Application::update()
{
	Mat3 trans =/*m.Translate(-100, -100)**/m.Rotate(ang);
	//Mat3 trans2 = trans * m.Translate(100, 100);
	
	vert.clear();
	for (int i = 0; i < sierpin.vertices.size(); ++i) {
		Vec3 v = trans * sierpin.vertices[i];
		vert.push_back(v);
	}
	ang++;
}

void Application::draw()
{
	clearScreen();
		for (int i = 0; i < vert.size() - 1; i += 3) {
			
			moveTo((int)vert[i].x(), (int)vert[i].y());
			lineTo((int)vert[i + 1].x(), (int)vert[i + 1].y());
			lineTo((int)vert[i + 2].x(), (int)vert[i + 2].y());
			lineTo((int)vert[i].x(), (int)vert[i].y());
			putPixel(0, 0, 255, 255, 255, 255);

		}
		
}

void Application::line(int x0, int x1, int y0, int y1) {

	int y = y0;
	int x = x0;
	
	int dx = x1 - x0;
	int dy = y1 - y0;
	
	// 1
	if (dx >= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

	int A = (y1 - y0);
	int B = (x1 - x0);
	int dInicial = (A + B / 2); //el punto de partida del algoritmo
	int d = dInicial;
	putPixel(x, y, 255, 0, 0, 255);
		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		
		}
	}
	//2
	else if (dx >= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//3
	else if (dx <= 0 && dy >= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = (y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 - 1; y <= y1; y++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//4
	else if (dx <= 0 && dy >= 0 && abs(dx) >= abs(dy)) {

		int A = (y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//5
	else if (dx <= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = -(x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 - 1; x >= x1; x--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);
		}
	}
	//6
	else  if (dx <= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = -(x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}
	//7
	else if (dx >= 0 && dy <= 0 && abs(dx) <= abs(dy)) {

		int A = (x1 - x0);
		int B = -(y1 - y0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (y = y0 + 1; y >= y1; y--) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				x++;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}
	//8
	else if (dx >= 0 && dy <= 0 && abs(dx) >= abs(dy)) {

		int A = -(y1 - y0);
		int B = (x1 - x0);
		int dInicial = (A + B / 2); //el punto de partida del algoritmo
		int d = dInicial;

		for (x = x0 + 1; x <= x1; x++) {

			if (d < 0) {
				d = d + A;
			}
			else {
				d += (A - B);
				y--;
			}
			//std::cout << x << "," << y << std::endl;
			putPixel(x, y, 255, 0, 0, 255);

		}
	}		

	
}

void Application::lineTo(int x, int y) {
	line(CuX, x, CuY, y);
	moveTo(x, y);
}

void Application::moveTo(int x, int y) {
//	int Cx = WIDTH / 2;
//	int Cy = HEIGHT / 2;
	CuX = x;
	CuY = y;
}

void Application::clearScreen() {
	for (int i = 0; i < HEIGHT - 1; i++) {
		for (int l = 0; l < WIDTH - 1; l++) {
			putPixel_0(l, i, 0, 0, 0, 0);
		}
	}
	/*for (int i = 0; i < BaseApplication::WIDTH * BaseApplication::HEIGHT * BaseApplication::RGBA; i += 1) {
		BaseApplication::_screenBuffer[i] = 0;
	}*/
}

void Application::setColor(unsigned char R, unsigned char G, unsigned char B, unsigned char A)
{
	Red = R;
	Green = G;
	Blue = B;
	Alpha = A;
}

